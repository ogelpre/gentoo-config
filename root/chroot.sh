#!/usr/bin/env bash

set -e

function print_help() {
  echo "-h print help"
  echo "-c chroot base"
}

while getopts "hc:" opt; do
  case $opt in
    h)
      print_help
      exit 1
      ;;
    c)
      CHROOT_BASE="${OPTARG}"
      ;;
    \?)
      if [ "${OPTARG}z" != "z" ]; then
        echo "Invalid option: -$OPTARG" >&2
      fi
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

if [ -z "${CHROOT_BASE}" ]; then
  echo "chroot base not set!"
  exit 1
fi

if ! mountpoint "${CHROOT_BASE}/dev" >/dev/null ; then
  mount --rbind /dev "${CHROOT_BASE}/dev"
  mount --make-rslave "${CHROOT_BASE}/dev"
fi

if ! mountpoint "${CHROOT_BASE}/proc" >/dev/null ; then
  mount -t proc /proc "${CHROOT_BASE}/proc"
fi

if ! mountpoint "${CHROOT_BASE}/sys" >/dev/null ; then
  mount --rbind /sys "${CHROOT_BASE}/sys"
  mount --make-rslave "${CHROOT_BASE}/sys"
fi

if ! mountpoint "${CHROOT_BASE}/tmp" >/dev/null ; then
  mount -t tmpfs tmpfs -o rw,noatime,nodev,nosuid,size=4G,mode=1777 "${CHROOT_BASE}/tmp"
fi

if ! mountpoint "${CHROOT_BASE}/var/tmp" >/dev/null ; then
  mount -t tmpfs tmpfs -o rw,size=8G,uid=portage,gid=portage,mode=775,noatime "${CHROOT_BASE}/var/tmp"
fi

cp -L /etc/resolv.conf "${CHROOT_BASE}/etc/"

chroot "${CHROOT_BASE}" /bin/bash --init-file /root/chroot-init.sh

umount -R "${CHROOT_BASE}/dev"
umount -R "${CHROOT_BASE}/proc"
umount -R "${CHROOT_BASE}/sys"
umount -R "${CHROOT_BASE}/tmp"
umount -R "${CHROOT_BASE}/var/tmp"
