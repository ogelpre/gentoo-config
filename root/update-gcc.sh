#!/usr/bin/env bash

eix-sync

emerge -1u portage

emerge -1u gcc
gcc-config --list-profiles
read -p "Select gcc version: " -r
gcc-config "${REPLY}"
source /chroot-init.sh
emerge -1 sys-devel/libtool
emerge --depclean
revdep-rebuild

emerge -uNDavt @world --with-bdeps=y
emerge --depclean
emerge @preserved-rebuild
revdep-rebuild

#rm -rf /var/distfiles/*

