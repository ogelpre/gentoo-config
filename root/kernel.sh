#!/usr/bin/env bash

set -e

SRC_DIR="/usr/src/linux"
BOOT_DIR="/boot"
JOBS="$(portageq envvar MAKEOPTS | sed 's|-j\s*\(\d*\)\D*|\1|')"
NICE_CMD="nice -n 19 ionice -c 3"
CCACHE_DIR="${SRC_DIR}.ccache"
CCACHE_PATH="/usr/lib/ccache/bin:${PATH}"

function print_help() {
  echo "-h print help"
  echo "-s kernel source dir (default: ${SRC_DIR})"
  echo "-b boot directory (default: ${BOOT_DIR})"
  echo "-j parallel jobs (default: ${JOBS})"
  echo "-c copy /proc/config.gz"
  echo "-m run make"
  echo "-i run make install"
  echo "-e run emerge @module-rebuild"
  echo "-d run dracut"
}

while getopts "hs:b:j:cmied" opt; do
  case $opt in
    h)
      print_help
      exit 1
      ;;
    s)
      SRC_DIR="${OPTARG}"
      ;;
    b)
      BOOT_DIR="${OPTARG}"
      ;;
    j)
      JOBS="${OPTARG}"
      ;;
    c)
      COPY_CONFIG="true"
      ;;
    m)
      DO_MAKE="true"
      ;;
    i)
      DO_INSTALL="true"
      ;;
    e)
      DO_EMERGE="true"
      ;;
    d)
      DO_DRACUT="true"
      ;;
    \?)
      if [ "${OPTARG}z" != "z" ]; then
        echo "Invalid option: -$OPTARG" >&2
      fi
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

#if [ "z" != "${@}z" ]; then
#  echo "unknown parameter ${@}"
#  exit 1
#fi

if [ "${DO_MAKE}z" = "z" -a "${DO_INSTALL}z" = "z" -a "${DO_EMERGE}z" = "z" -a "${DO_DRACUT}z" = "z" ]; then
  DO_MAKE="true"
  DO_INSTALL="true"
  DO_EMERGE="true"
  DO_DRACUT="true"
fi

pushd "${SRC_DIR}"
KERNEL_VERSION=$(readlink -f . | sed "s|[^-]*-\(.*\)|\1|")

if ! mountpoint "${BOOT_DIR}" >/dev/null ; then
  mount "${BOOT_DIR}"
  unmount="true"
fi

if [ "${COPY_CONFIG}" = "true" -o ! -e .config ]; then
    zcat /proc/config.gz > .config
fi

if [ "${DO_MAKE}" = "true" ]; then
  make oldconfig
  make menuconfig
  #CCACHE_DIR="${CCACHE_DIR}" PATH="${CCACHE_PATH}" ${NICE_CMD} make -j "${JOBS}"
  ${NICE_CMD} make -j "${JOBS}"
  #CCACHE_DIR="${CCACHE_DIR}" ccache -c
fi

if [ "${DO_INSTALL}" = "true" ]; then
  make modules_install
  make install
fi

if [ "${DO_EMERGE}" = "true" ]; then
  emerge --quiet --ask n --usepkg n @module-rebuild
fi

if [ "${DO_DRACUT}" = "true" ]; then
  dracut --force --kver "${KERNEL_VERSION}"
  #INITRAMFS_VERSION=$(basename $(readlink -f "${BOOT_DIR}/initramfs") | sed "s|[^-]*-\(.*\).img|\1|")
  mv "${BOOT_DIR}/initramfs" "${BOOT_DIR}/initramfs.old"
  ln -sf "initramfs-${KERNEL_VERSION}.img" "${BOOT_DIR}/initramfs"
fi

if [ "${unmount}" = "true" ]; then
  umount "${BOOT_DIR}"
fi

popd
